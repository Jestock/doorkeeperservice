using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using AutoMapper;
using AutoMapper.Extensions.ExpressionMapping;
using Clay.DoorKeeper.Api.Config;
using Clay.DoorKeeper.Api.Mapping;
using Clay.DoorKeeper.Api.Middleware;
using Clay.DoorKeeper.Api.Security;
using Clay.DoorKeeper.Api.Security.Implementation;
using Clay.DoorKeeper.Core;
using Clay.DoorKeeper.Core.Services;
using Clay.DoorKeeper.Data;
using Clay.DoorKeeper.Data.Context;
using Clay.DoorKeeper.Data.Mapping;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Clay.DoorKeeper.Api
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        private readonly IConfiguration configuration;

        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var envConfig = GetEnvironmentConfiguration(services);
            var jwtSecret = Encoding.ASCII.GetBytes(envConfig.JwtSecret);

            services.AddAutoMapper(config => config.AddExpressionMapping(),
                typeof(DataAccessMappingProfile),
                typeof(ApiMappingProfile));

            services.AddDomainLayer();
            services.AddDataAccessLayer(configuration);
            services.AddScoped<ITokenProvider, TokenProvider>();

            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(jwtBearerOptions =>
                {
                    SetupTokenValidation(jwtBearerOptions, jwtSecret);
                });

            AddMvcCoreComponents(services);

            services.AddHealthChecks();
            
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            app.UseHsts()
                .UseHttpsRedirection()
                .UseRouting()
                .UseAuthentication()
                .UseAuthorization()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapControllers();
                    endpoints.MapHealthChecks("/health");
                });

            app.UseExceptionLogging();

            ApplyMigrations(app);
        }

        /// <remarks>
        /// Warning: this is for demonstration purposes only.
        /// This method in startup class breaks the separation of concerns principle.
        /// Moreover, migrations should be handled as a separate step on the CI/CD pipeline.
        /// </remarks>
        private static void ApplyMigrations(IApplicationBuilder app)
        {
            using var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
            using var context = scope.ServiceProvider.GetService<DoorKeeperDataContext>();

            context.Database.Migrate();
        }

        private static void AddMvcCoreComponents(IServiceCollection services)
        {
            services.AddMvcCore()
                .SetCompatibilityVersion(CompatibilityVersion.Latest)
                .AddApiExplorer()
                .AddFormatterMappings()
                .AddDataAnnotations()
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.Converters.Add(new StringEnumConverter
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    });
                })
                .AddAuthorization();
        }

        private EnvironmentConfiguration GetEnvironmentConfiguration(IServiceCollection services)
        {
            var envConf = configuration.GetSection(nameof(EnvironmentConfiguration));
            services.Configure<EnvironmentConfiguration>(envConf);

            return envConf.Get<EnvironmentConfiguration>();
        }

        private static void SetupTokenValidation(JwtBearerOptions options, byte[] jwtSecret)
        {
            options.Events = JwtBearerEventsOptions();
            options.RequireHttpsMetadata = false;
            options.SaveToken = true;
            options.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(jwtSecret),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        }

        private static JwtBearerEvents JwtBearerEventsOptions()
        {
            return new JwtBearerEvents
            {
                OnTokenValidated = context =>
                {
                    var userService = context.HttpContext.RequestServices.GetRequiredService<IUserService>();
                    var userId = long.Parse(context.Principal.Identity.Name);
                    var user = userService.GetByIdAsync(userId, CancellationToken.None).Result;
                    if (user == null)
                    {
                        context.Fail("Unauthorized");
                    }

                    return Task.CompletedTask;
                }
            };
        }
    }
}
