namespace Clay.DoorKeeper.Api.Config
{
    public class EnvironmentConfiguration
    {
        public string JwtSecret { get; set; }
        public int ExpirationMinutes { get; set; }
    }
}
