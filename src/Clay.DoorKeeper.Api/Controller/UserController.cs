using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clay.DoorKeeper.Api.Models;
using Clay.DoorKeeper.Api.Security;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clay.DoorKeeper.Api.Controller
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly ITokenProvider tokenProvider;
        private readonly IMapper mapper;

        public UsersController(IUserService userService, ITokenProvider tokenProvider, IMapper mapper)
        {
            this.userService = userService;
            this.tokenProvider = tokenProvider;
            this.mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetAll(CancellationToken token)
        {
            var users = await userService.GetAllAsync(token);

            return Ok(mapper.Map<IEnumerable<UserModel>>(users));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel>> GetById(long id, CancellationToken token)
        {
            var user = await userService.GetByIdAsync(id, token);

            return Ok(mapper.Map<UserModel>(user));
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public async Task<ActionResult<UserModel>> Authenticate([FromBody] [Required] AuthenticateModel model,
            CancellationToken token)
        {
            try
            {
                var user = await userService.AuthenticateAsync(model.Username, model.Password, token);

                var result = mapper.Map<UserModel>(user);
                result.Token = tokenProvider.GenerateToken(result.Id, result.Username);

                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] [Required] RegisterModel model, CancellationToken token)
        {
            try
            {
                await userService.CreateAsync(mapper.Map<User>(model), model.Password, token);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("{id}")]
        public IActionResult Update(long id, [FromBody] UpdateModel model, CancellationToken token)
        {
            try
            {
                var user = mapper.Map<User>(model);
                user.Id = id;

                var result = userService.UpdateAsync(user, model.Password, token);

                return Ok(mapper.Map<UserModel>(result));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id, CancellationToken token)
        {
            userService.DeleteAsync(id, token);

            return Ok();
        }
    }
}
