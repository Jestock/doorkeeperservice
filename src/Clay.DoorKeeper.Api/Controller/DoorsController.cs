using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Clay.DoorKeeper.Api.Controller
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class DoorsController : ControllerBase
    {
        /// <remarks>
        /// There should be the code that eventually will call door low-level API or
        /// something that can communicate with the door directly.
        /// For simplicity, in case of successful authorization, a simple message is returned.
        /// </remarks>
        [HttpPut("{id}")]
        public ActionResult<string> Open([FromRoute] [Required] int id)
        {
            /*
             
            */
            return $"The door #{id} is opened";
        }
    }
}
