using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace Clay.DoorKeeper.Api.Middleware
{
    public class ExceptionLoggingMiddleware
    {
        private const string ApplicationJsonMimeType = "application/json";

        private readonly ILogger<ExceptionLoggingMiddleware> logger;
        private readonly RequestDelegate next;

        public ExceptionLoggingMiddleware(RequestDelegate next, ILogger<ExceptionLoggingMiddleware> logger)
        {
            this.next = next;
            this.logger = logger;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                logger.LogError(exception, "Unhandled error");

                context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                context.Response.ContentType = ApplicationJsonMimeType;
            }
        }
    }
}
