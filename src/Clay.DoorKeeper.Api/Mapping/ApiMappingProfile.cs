using AutoMapper;
using Clay.DoorKeeper.Api.Models;
using Clay.DoorKeeper.Core.DomainModels;

namespace Clay.DoorKeeper.Api.Mapping
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<RegisterModel, User>();
            CreateMap<AuthenticateModel, User>();
            CreateMap<UpdateModel, User>();
        }
    }
}
