using System.ComponentModel.DataAnnotations;

namespace Clay.DoorKeeper.Api.Models
{
    public class RegisterModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
