namespace Clay.DoorKeeper.Api.Models
{
    public class UserModel
    {
        public long Id { get; set; }

        public string Username { get; set; }

        public string Token { get; set; }
    }
}
