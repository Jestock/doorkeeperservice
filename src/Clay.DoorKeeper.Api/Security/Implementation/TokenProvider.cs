using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Clay.DoorKeeper.Api.Config;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Clay.DoorKeeper.Api.Security.Implementation
{
    public class TokenProvider : ITokenProvider
    {
        private readonly EnvironmentConfiguration configuration;

        public TokenProvider(IOptions<EnvironmentConfiguration> configuration)
        {
            this.configuration = configuration.Value;
        }

        public string GenerateToken(long userId, string userName)
        {
            var secret = Encoding.ASCII.GetBytes(configuration.JwtSecret);
            var securityKey = new SymmetricSecurityKey(secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, userId.ToString())
                }),
                Expires = DateTime.UtcNow.AddMinutes(configuration.ExpirationMinutes),
                SigningCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return tokenString;
        }
    }
}
