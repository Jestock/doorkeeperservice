namespace Clay.DoorKeeper.Api.Security
{
    public interface ITokenProvider
    {
        string GenerateToken(long userId, string userName);
    }
}
