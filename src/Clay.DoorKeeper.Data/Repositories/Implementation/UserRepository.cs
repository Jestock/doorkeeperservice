using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Repositories;
using Clay.DoorKeeper.Core.Specifications;
using Clay.DoorKeeper.Data.Context;
using Clay.DoorKeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clay.DoorKeeper.Data.Repositories.Implementation
{
    public class UserRepository : IUserRepository
    {
        private readonly DoorKeeperDataContext dataContext;
        private readonly IMapper mapper;
        private readonly DbSet<UserEntity> users;

        public UserRepository(DoorKeeperDataContext dataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
            this.mapper = mapper;

            users = dataContext.Users;
        }

        public async Task<IEnumerable<User>> GetAllAsync(CancellationToken token)
        {
            return await mapper.ProjectTo<User>(users).ToListAsync(token);
        }

        public async Task<User> GetByIdAsync(long id, CancellationToken token)
        {
            var userEntity = await users.SingleOrDefaultAsync(x => x.Id == id, token);

            return mapper.Map<User>(userEntity);
        }

        public async Task<User> GetBySpecificationAsync(UserSpecification specification, CancellationToken token)
        {
            var query = users.AsQueryable();
            if (!string.IsNullOrWhiteSpace(specification.Username))
            {
                query = query.Where(x => x.Username == specification.Username);
            }

            var user = await query.FirstOrDefaultAsync(token);

            return mapper.Map<User>(user);
        }
        
        public async Task<long> CreateAsync(User user, CancellationToken token)
        {
            var userEntity = mapper.Map<UserEntity>(user);

            users.Add(userEntity);
            await dataContext.SaveChangesAsync(token);

            return userEntity.Id;
        }

        public async Task<User> UpdateAsync(User user, CancellationToken token)
        {
            var userEntity = await users.SingleOrDefaultAsync(x => x.Id == user.Id, token);
            userEntity = mapper.Map(user, userEntity);

            await dataContext.SaveChangesAsync(token);

            return mapper.Map<User>(userEntity);
        }

        public async Task DeleteAsync(long id, CancellationToken token)
        {
            var userEntity = await users.SingleOrDefaultAsync(x => x.Id == id, token);
            if (userEntity != null)
            {
                users.Remove(userEntity);
            }
        }
    }
}
