using System.Diagnostics.CodeAnalysis;

using Clay.DoorKeeper.Core.Repositories;
using Clay.DoorKeeper.Data.Context;
using Clay.DoorKeeper.Data.Repositories.Implementation;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Clay.DoorKeeper.Data
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataAccessLayer(this IServiceCollection services, IConfiguration configuration)
        {
            var assembly = typeof (DoorKeeperDataContext).Assembly;
            var connection = configuration.GetConnectionString("Database");

            services.AddDbContext<DoorKeeperDataContext>(options
                => options.UseNpgsql(connection, ob => ob.MigrationsAssembly(assembly.GetName().Name).EnableRetryOnFailure()));

            services.AddTransient<IUserRepository, UserRepository>();

            services.AddHealthChecks().AddDbContextCheck<DoorKeeperDataContext>(failureStatus: HealthStatus.Degraded);

            return services;
        }
    }
}
