using AutoMapper;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Data.Entities;

namespace Clay.DoorKeeper.Data.Mapping
{
    public class DataAccessMappingProfile : Profile
    {
        public DataAccessMappingProfile()
        {
            CreateMap<UserEntity, User>()
                .ForMember(dto => dto.HashAndSalt, opt => opt.Ignore())
                .AfterMap((entity, model) => model.HashAndSalt = new HashAndSalt
                {
                    Hash = entity.PasswordHash,
                    Salt = entity.PasswordSalt
                });

            CreateMap<User, UserEntity>()
                .ForMember(dto => dto.PasswordHash, opt => opt.MapFrom(x => x.HashAndSalt.Hash))
                .ForMember(dto => dto.PasswordSalt, opt => opt.MapFrom(x => x.HashAndSalt.Salt));
        }
    }
}