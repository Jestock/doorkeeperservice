using System.Reflection;
using Clay.DoorKeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Clay.DoorKeeper.Data.Context
{
    public class DoorKeeperDataContext : DbContext
    {
        public DbSet<UserEntity> Users { get; set; }

        public DoorKeeperDataContext(DbContextOptions<DoorKeeperDataContext> options) : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
        }
    }
}
