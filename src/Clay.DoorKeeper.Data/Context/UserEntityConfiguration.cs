using Clay.DoorKeeper.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Clay.DoorKeeper.Data.Context
{
    public class UserEntityConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        private readonly string tableName = nameof(UserEntity);

        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.ToTable(tableName);
            
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.Username).HasName(tableName + "_userName_index").IsUnique();

            builder.Property(x => x.Username)
                   .HasColumnType("varchar")
                   .HasMaxLength(100)
                   .HasColumnName(nameof(UserEntity.Username.ToLowerInvariant));
        }
    }
}
