using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Specifications;

namespace Clay.DoorKeeper.Core.Repositories
{
    public interface IUserRepository
    {
        Task<long> CreateAsync(User user, CancellationToken token);

        Task<IEnumerable<User>> GetAllAsync(CancellationToken token);
        Task<User> GetByIdAsync(long id, CancellationToken token);
        Task<User> GetBySpecificationAsync(UserSpecification specification, CancellationToken token);

        Task<User> UpdateAsync(User user, CancellationToken token);

        Task DeleteAsync(long id, CancellationToken token);
    }
}
