namespace Clay.DoorKeeper.Core.Messages
{
    public static class ValidationErrors
    {
        public const string PasswordIsEmpty = "Password cannot be null or empty";
        public const string InvalidHash = "Invalid length of password hash";
        public const string InvalidSalt = "Invalid length of password salt";


        public const string UserNotfound = "User not found";
        public const string UsernameIsNotUnique = "Username is already taken";
        public const string InvalidUsernameOrPassword = "Username or password is incorrect";
    }
}
