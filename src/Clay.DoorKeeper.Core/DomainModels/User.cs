namespace Clay.DoorKeeper.Core.DomainModels
{
    public class User
    {
        public long Id { get; set; }

        public string Username { get; set; }

        public HashAndSalt HashAndSalt { get; set; }
    }
}
