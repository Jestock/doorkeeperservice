namespace Clay.DoorKeeper.Core.DomainModels
{
    public class HashAndSalt
    {
        public byte[] Hash { get; set; }

        public byte[] Salt { get; set; }
    }
}
