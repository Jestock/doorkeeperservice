namespace Clay.DoorKeeper.Core.Specifications
{
    // Extension point - simple Query Object pattern implementation.
    // If new search criteria emerge, this object will incapsulate them   
    public class UserSpecification
    {
        public string Username { get; set; }
    }
}
