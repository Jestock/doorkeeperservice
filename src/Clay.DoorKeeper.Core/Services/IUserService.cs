using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Clay.DoorKeeper.Core.DomainModels;

namespace Clay.DoorKeeper.Core.Services
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetAllAsync(CancellationToken token);

        Task<User> GetByIdAsync(long id, CancellationToken token);

        Task<User> AuthenticateAsync(string username, string password, CancellationToken token);

        Task<User> CreateAsync(User user, string password, CancellationToken token);

        Task<User> UpdateAsync(User user, string password, CancellationToken token);

        Task DeleteAsync(long id, CancellationToken token);
    }
}
