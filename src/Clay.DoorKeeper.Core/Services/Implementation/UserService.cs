using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Exceptions;
using Clay.DoorKeeper.Core.Repositories;
using Clay.DoorKeeper.Core.Specifications;
using static Clay.DoorKeeper.Core.Messages.ValidationErrors;

namespace Clay.DoorKeeper.Core.Services.Implementation
{
    public class UserService : IUserService
    {
        private readonly IUserRepository repository;
        private readonly IPasswordHandler passwordHandler;

        public UserService(IUserRepository repository, IPasswordHandler passwordHandler)
        {
            this.repository = repository;
            this.passwordHandler = passwordHandler;
        }

        public async Task<IEnumerable<User>> GetAllAsync(CancellationToken token)
        {
            return await repository.GetAllAsync(token);
        }

        public async Task<User> GetByIdAsync(long id, CancellationToken token)
        {
            return await repository.GetByIdAsync(id, token);
        }

        public async Task<User> AuthenticateAsync(string username, string password, CancellationToken token)
        {
            var spec = new UserSpecification { Username = username };
            
            var user = await repository.GetBySpecificationAsync(spec, token);
            if (user == null)
            {
                throw new ValidationException(UserNotfound);
            }
            
            if (!passwordHandler.VerifyPasswordHash(password, user.HashAndSalt))
            {
                throw new ValidationException(InvalidUsernameOrPassword);
            }

            return user;
        }

        public async Task<User> CreateAsync(User user, string password, CancellationToken token)
        {
            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ValidationException(PasswordIsEmpty);
            }

            var spec = new UserSpecification { Username = user.Username };
            var result = await repository.GetBySpecificationAsync(spec, token);
            if (result != null)
            {
                throw new ValidationException(UsernameIsNotUnique);
            }

            user.HashAndSalt = passwordHandler.GeneratePasswordHash(password);
            user.Id = await repository.CreateAsync(user, token);

            return user;
        }

        public async Task<User> UpdateAsync(User user, string password, CancellationToken token)
        {
            var existingUser = await repository.GetByIdAsync(user.Id, token);
            if (existingUser == null)
            {
                throw new ValidationException(UserNotfound);
            }

            if (!string.IsNullOrWhiteSpace(user.Username) && user.Username != existingUser.Username)
            {
                var spec = new UserSpecification { Username = user.Username };
                var result = await repository.GetBySpecificationAsync(spec, token);
                if (result != null)
                {
                    throw new ValidationException(UsernameIsNotUnique);
                }

                existingUser.Username = user.Username;
            }

            if (!string.IsNullOrWhiteSpace(password))
            {
                existingUser.HashAndSalt = passwordHandler.GeneratePasswordHash(password);
            }

            return await repository.UpdateAsync(user, token);
        }

        public async Task DeleteAsync(long id, CancellationToken token)
        {
            await repository.DeleteAsync(id, token);
        }
    }
}