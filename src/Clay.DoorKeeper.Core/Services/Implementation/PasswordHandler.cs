using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Exceptions;
using static Clay.DoorKeeper.Core.Messages.ValidationErrors;

namespace Clay.DoorKeeper.Core.Services.Implementation
{
    public class PasswordHandler : IPasswordHandler
    {
        public HashAndSalt GeneratePasswordHash(string password)
        {
            if (password == null || string.IsNullOrWhiteSpace(password))
            {
                throw new ValidationException(PasswordIsEmpty, nameof(password));
            }

            using var hmac = new HMACSHA512();

            return new HashAndSalt
            {
                Salt = hmac.Key,
                Hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password))
            };
        }

        public bool VerifyPasswordHash(string password, HashAndSalt hashAndSalt)
        {
            if (password == null || string.IsNullOrWhiteSpace(password))
            {
                throw new ValidationException(PasswordIsEmpty, nameof(password));
            }

            if (hashAndSalt.Hash.Length != 64)
            {
                throw new ValidationException(InvalidHash, nameof(hashAndSalt));
            }

            if (hashAndSalt.Salt.Length != 128)
            {
                throw new ValidationException(InvalidSalt, nameof(hashAndSalt));
            }

            using var hmac = new HMACSHA512(hashAndSalt.Salt);
            var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

            return computedHash.SequenceEqual(hashAndSalt.Hash);
        }
    }
}
