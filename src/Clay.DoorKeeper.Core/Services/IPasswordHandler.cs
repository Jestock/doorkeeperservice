using Clay.DoorKeeper.Core.DomainModels;

namespace Clay.DoorKeeper.Core.Services
{
    public interface IPasswordHandler
    {
        HashAndSalt GeneratePasswordHash(string password);

        bool VerifyPasswordHash(string password, HashAndSalt hashAndSalt);
    }
}
