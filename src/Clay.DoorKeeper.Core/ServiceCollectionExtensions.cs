using System.Diagnostics.CodeAnalysis;
using Clay.DoorKeeper.Core.Services;
using Clay.DoorKeeper.Core.Services.Implementation;
using Microsoft.Extensions.DependencyInjection;

namespace Clay.DoorKeeper.Core
{
    [ExcludeFromCodeCoverage]
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDomainLayer(this IServiceCollection services)
        {
            services.AddTransient<IPasswordHandler, PasswordHandler>();
            services.AddTransient<IUserService, UserService>();

            return services;
        }
    }
}
