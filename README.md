# DoorKeeper service

## Set up the application
 
 1. Run `docker-compose -f docker-compose-db-only.yaml up` form the solution directory. 
 2. Then, run `dotnet run --project .\src\Clay.DoorKeeper.Api\Clay.DoorKeeper.Api.csproj --configuration Development`

Now the application up and running.

## The Application overview
The application is the REST API which provides an ability to register users, authenticate them and request access to the doors of the office.
JWT is used to provide an authorization flow. Once the user is successfully authenticated he receives the access token in the JWT format. This access token then is used in every further user request.  
There should be the code that eventually will call the door low-level API or something that can communicate with the door directly. For simplicity, all that low-level communication is omitted.

## Component architecture
The application follows the onion architecture. There are 3 main layers and each of layers serves one purpose, shortly:  

 1. API (Application) layer - Provides an interface (contract) to communicate with the outer world. In this case, the REST API is used.  
 2. Core (Domain) Layer - The core of the system. This is the reason why the application is written - the logic of the product. This layer should not have any dependencies on any other layers. The interface (contract) of the necessary data should be declared here.  
 3. Data Access Layer - Provides a data access infrastructure.  

 Additional layers are possible but in this case, I omit them intentionally, to keep the application simple.  
 The communication between layers should be declared explicitly. In this case, I use automapper to convert/map between entities and domains and domains and view models.

## Technology stack considerations
 1. Docker. Facilitates integration with CI/CD pipelines. Nowadays it's an industry-standard de facto.
 2. PostgreSQL. It's mature open source database and a platform.
 3. ASP.NET Core 3.1 - brand new ASP.NET, the LTS version.
 4. Entity Framework Core 3.1 - the same as for ASP.NET
 5. Serilog - Customizable logging tool with structural logging out of the box, provides plenty of extensions (enrichers) and a lot of integrations (sinks)
 6. xUnit, WebApplicationFactory, Moq, AutoFixture - a toolset for writing fast, isolated, robust and fully automated unit tests.

## The list of unresolved issues 
 1. Hosting ASP.NET Core application with Docker over HTTPS. There is an issue to provide for the Kestrel a signed certificate. 
 2. Implementation claims-based authorization. This enables access customization.
 3. Integration with external authentication Providers.
 4. Providing the app with integration tests (WebApplicationFactory). It's really helpful for the API layer and startup testing.
 5. Absence of swagger declaration. Swagger (OpenApi) provides a convenient declaration of an API.