FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS publish
COPY src/ /src/
COPY test/ /test/
COPY Clay.DoorKeeper.sln .

# Restore NuGet packages using NuGet.config
RUN dotnet restore "Clay.DoorKeeper.sln"
# Build/publish application
RUN dotnet publish "Clay.DoorKeeper.sln" -c Release -o /app --no-restore

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim
WORKDIR /app
COPY --from=publish /app .

RUN groupadd -g 1500 app  \
    && useradd -u 1500 -g 1500 app \
    && chown app:app -R /app
USER app:app
CMD [ "dotnet", "Clay.DoorKeeper.Api.dll" ]