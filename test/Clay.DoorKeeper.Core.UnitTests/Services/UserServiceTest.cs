using AutoFixture;
using Clay.DoorKeeper.Core.DomainModels;
using Clay.DoorKeeper.Core.Exceptions;
using Clay.DoorKeeper.Core.Messages;
using Clay.DoorKeeper.Core.Repositories;
using Clay.DoorKeeper.Core.Services;
using Clay.DoorKeeper.Core.Services.Implementation;
using Clay.DoorKeeper.Core.Specifications;
using FluentAssertions;
using Moq;

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Clay.DoorKeeper.Core.UnitTests.Services
{
    public class UserServiceTest
    {
        private readonly Fixture fixture;
        private readonly UserService userService;
        private readonly Mock<IUserRepository> repository;
        private readonly Mock<IPasswordHandler> passwordHandler;

        public UserServiceTest()
        {
            fixture = new Fixture();

            repository = new Mock<IUserRepository>();
            passwordHandler = new Mock<IPasswordHandler>();

            userService = new UserService(repository.Object, passwordHandler.Object);
        }

        [Fact(DisplayName = "Returns all the users")]
        public async Task GetAllAsync()
        {
            var expectedUsers = new List<User>
            {
                fixture.Create<User>(),
                fixture.Create<User>(),
                fixture.Create<User>()
            };

            repository.Setup(x => 
                x.GetAllAsync(It.IsAny<CancellationToken>())).ReturnsAsync(expectedUsers);
            
            var result = await userService.GetAllAsync(CancellationToken.None);

            result.Should().BeEquivalentTo(expectedUsers);
        }

        [Fact(DisplayName = "Returns a user by Id")]
        public async Task GetByIdAsync()
        {
            var expectedUser = fixture.Create<User>();
            
            repository.Setup(x => 
                x.GetByIdAsync(It.IsAny<long>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedUser);
            
            var result = await userService.GetByIdAsync(expectedUser.Id, CancellationToken.None);

            result.Should().Be(expectedUser);
        }
        
        [Fact(DisplayName = "Returns a authenticated user")]
        public async Task AuthenticateAsync01()
        {
            var password = fixture.Create<string>();
            var expectedUser = fixture.Build<User>()
                .With(x => x.HashAndSalt, () => fixture.Create<HashAndSalt>())
                .Create();

            repository.Setup(x => 
                    x.GetBySpecificationAsync(It.IsAny<UserSpecification>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedUser);

            passwordHandler.Setup(x =>
                    x.VerifyPasswordHash(It.IsAny<string>(), It.IsAny<HashAndSalt>()))
                .Returns(true);

            var result = await userService.AuthenticateAsync(expectedUser.Username, password, CancellationToken.None);

            result.Should().BeEquivalentTo(expectedUser);
        }

        [Fact(DisplayName = "Throws an exception if the user doesn't exist")]
        public async Task AuthenticateAsync02()
        {
            var password = fixture.Create<string>();
            var userName = fixture.Create<string>();

            Func<Task> act = async () =>
            {
                await userService.AuthenticateAsync(userName, password, CancellationToken.None);
            };

            await act.Should().ThrowAsync<ValidationException>().WithMessage(ValidationErrors.UserNotfound);
        }
        
        [Fact(DisplayName = "Throws an exception if password validations fails")]
        public async Task AuthenticateAsync03()
        {
            var password = fixture.Create<string>();
            var expectedUser = fixture.Create<User>();

            repository.Setup(x => 
                    x.GetBySpecificationAsync(It.IsAny<UserSpecification>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedUser);

            Func<Task> act = async () =>
            {
                await userService.AuthenticateAsync(expectedUser.Username, password, CancellationToken.None);
            };

            await act.Should().ThrowAsync<ValidationException>().WithMessage(ValidationErrors.InvalidUsernameOrPassword);
        }

        [Fact(DisplayName = "Returns a created user")]
        public async Task CreateAsync01()
        {
            var password = fixture.Create<string>();
            var hashAndSalt = fixture.Create<HashAndSalt>();
            
            var expectedUser = fixture.Build<User>()
                .With(x => x.HashAndSalt, () => hashAndSalt)
                .Create();

            repository.Setup(x => 
                x.CreateAsync(It.IsAny<User>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedUser.Id);

            passwordHandler.Setup(x =>
                    x.GeneratePasswordHash(It.IsAny<string>()))
                .Returns(hashAndSalt);

            var result = await userService.CreateAsync(expectedUser, password, CancellationToken.None);
            result.Should().BeEquivalentTo(expectedUser);
        }
        
        [Fact(DisplayName = "Throws an exception if a password is not provided")]
        public async Task CreateAsync02()
        {
            var expectedUser = fixture.Create<User>();
            
            Func<Task> act = async () =>
            {
                await userService.CreateAsync(expectedUser, null, CancellationToken.None);
            };

            await act.Should().ThrowAsync<ValidationException>().WithMessage(ValidationErrors.PasswordIsEmpty);
        }
        
        [Fact(DisplayName = "Throws an exception if the provided name is not unique")]
        public async Task CreateAsync03()
        {
            var password = fixture.Create<string>();
            var expectedUser = fixture.Create<User>();

            repository.Setup(x => 
                    x.GetBySpecificationAsync(It.IsAny<UserSpecification>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(expectedUser);
            
            Func<Task> act = async () =>
            {
                await userService.CreateAsync(expectedUser, password, CancellationToken.None);
            };

            await act.Should().ThrowAsync<ValidationException>().WithMessage(ValidationErrors.UsernameIsNotUnique);
        }
    }
}
